require 'httparty'

class GithubFetcher

  API_BASE = 'https://api.github.com'

  attr_reader :username

  def initialize(username)
    @username = username
  end

  def repositories
    @repositories ||= fetch_repositories
  end

  def languages
    @languages ||= fetch_languages
  end

private

  def request_headers
    {
      'User-Agent' => username,
      'Authorization' => "token #{ENV['github_token']}"
    }
  end

  def user_repositories_url
    "#{API_BASE}/users/#{username}/repos"
  end

  def fetch_repositories
    response = HTTParty.get(user_repositories_url, headers: request_headers)
    response.code == 200 ? extract_repos_information(response) : nil
  end

  def extract_repos_information(response)
    hash = {}
    response.each do |repo|
      hash[repo['id']] = {
          name: repo['name'],
          languages_url: repo['languages_url']
        }
    end
    hash
  end

  def fetch_languages
    hash = {}
    repositories.each do |_, repo|
      repo_languages = fetch_languages_for_repo(repo)
      hash.merge!(repo_languages) { |_, prev, value| prev + value }
    end
    hash
  end

  def fetch_languages_for_repo(repo)
    response = HTTParty.get(repo[:languages_url], headers: request_headers)
  end

end
