class GithubController < ApplicationController

  def index; end

  def show
    @github = Github.new(username: username)
    if @github.valid?
      render json: @github.languages_usage
    else
      render json: @github.errors, status: 400
    end
  end

  private

  def username
    params[:username]
  end

end
