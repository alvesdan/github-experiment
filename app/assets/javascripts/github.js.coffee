class Github
  constructor: ->
    @input = $('#username')

  bindEnter: ->
    _this = this
    @input.on 'keyup', (event) ->
      if event.keyCode == 13
        event.preventDefault()
        _this.show($(this).val().trim())

  show: (username) ->
    _this = this
    _this.disableInput()
    _this.cleanPage()

    $.getJSON(username)
    .fail ->
      alert('Sorry, we could not fetch data for this user!')
    .done (data) ->
      _this.buildPage(data)
    .always ->
      _this.enableInput(username)

  disableInput: ->
    @input.val('Loading...').prop('disabled', true)

  enableInput: (username) ->
    @input.val(username).prop('disabled', false)

  cleanPage: ->
    $('table.empty').html('')

  buildPage: (data) ->
    for language, percentage of data
      tr = "<tr class=\"#{language}\"><td>#{percentage}%</td><td>#{language}</td></tr>"
      $('table.empty').prepend(tr)

$ ->
  github = new Github()
  github.bindEnter()
