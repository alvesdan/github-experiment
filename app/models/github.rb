class Github
  include ActiveModel::Model

  attr_accessor :username, :github_fetcher
  validates :username, presence: true
  validate :user_repositories

  delegate :repositories, :languages, to: :github_fetcher

  def initialize(username: )
    @username = username
    @github_fetcher = GithubFetcher.new(username)
  end

  # Moving usage method to Github class
  # These responsibilities are not from
  # the GithubFetcher

  def languages_usage
    Hash[
      languages.sort_by { |_, value| value }.map { |key, value|
          [key, percentage_for(value)]
        }
    ]
  end

private

  def user_repositories
    errors.add(:base, 'Could not fetch repositories data for this user.') unless repositories
  end

  def languages_total_sum
    languages.values.reduce(:+)
  end

  def percentage_for(value)
    (value * 100.00)/languages_total_sum
  end

end
