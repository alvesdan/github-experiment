require 'spec_helper'

describe GithubController do

  describe 'GET #index' do
    before { get :index }
    it { expect(response).to be_success }
    it { expect(response).to render_template('index') }
  end

  let(:mocked_github) { double(Github) }
  let(:mocked_response) do
    {
      "Ruby" => 40,
      "Javascript" => 30,
      "CSS" => 20,
      "Coffee" => 10
    }
  end

  def perform_get!
    get :show, username: 'alvesdan', format: :json
  end

  describe 'POST #show' do
    it 'instantiate a Github object' do
      Github.stub(new: double(valid?: false, errors: 'Errors'))
      expect(Github).to receive(:new).with(username: 'alvesdan')
      perform_get!
    end
    context 'when object is valid' do
      before do
        mocked_github.stub(valid?: true)
        Github.stub(new: mocked_github)
      end
      context 'when user has repositories' do
        it 'render a json with languages usage' do
          mocked_github.stub(languages_usage: mocked_response)
          perform_get!
          expect(JSON.parse(response.body)).to eq mocked_response
        end
      end
    end
    context 'when object is invalid' do
      before do
        mocked_github.stub(valid?: false)
        mocked_github.stub(errors: {base: 'Base error'})
        Github.stub(new: mocked_github)
        perform_get!
      end
      it { expect(response.code).to eq '400' }
      it { expect(JSON.parse(response.body)).to eq({'base' => 'Base error'}) }
    end
  end

end
