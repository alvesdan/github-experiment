require 'spec_helper'

describe Github do

  subject { Github.new(username: 'alvesdan') }
  let(:spec_languages) do
    {
      "Ruby" => 400,
      "Javascript" => 300,
      "CSS" => 200,
      "Coffee" => 100
    }
  end

  it { expect(subject).to be_kind_of(ActiveModel::Model) }
  it 'raises an exception when not sending username' do
    expect { Github.new }.to raise_error(ArgumentError)
  end

  it 'validates user repositories presence' do
    allow(subject).to receive(:repositories).and_return(nil)
    expect(subject.valid?).to be_false
  end

  it 'instantiate a github fetcher object' do
    expect(subject.github_fetcher).to be_kind_of(GithubFetcher)
  end

  describe '#languages_usage' do
    before { subject.stub(languages: spec_languages)}
    it 'should return percentage languages usage percentage' do
      expect(subject.languages_usage).to eq({
          "Ruby" => 40,
          "Javascript" => 30,
          "CSS" => 20,
          "Coffee" => 10
        })
    end
    it 'must sum a total of 100' do
      expect(subject.languages_usage.values.reduce(:+)).to eq 100
    end
  end

end
