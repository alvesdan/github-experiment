require 'spec_helper'

describe GithubFetcher, slow: true do

  before :all do
    @github_fetcher = GithubFetcher.new('alvesdan')
  end

  it 'initialize a instance with username' do
    expect(@github_fetcher.username).to eq 'alvesdan'
  end

  context 'when response code is 500' do
    # Simulating an API error
    before { allow(HTTParty).to receive(:get).and_return(double(code: 500)) }
    it 'returns nil' do
      expect(GithubFetcher.new('alvesdan').repositories).to be_nil
    end
  end

  context 'when response code is 200' do
    it 'returns an hash with repo information' do
      expect(@github_fetcher.repositories).to include({
          15122972 => {
            name: "coding-ruby",
            languages_url: "https://api.github.com/repos/alvesdan/coding-ruby/languages"
          }
        })
    end

    describe 'fecthing languages' do
      it 'fetch languages for repos' do
        expect(@github_fetcher.languages['Ruby']).to be_a_kind_of(Numeric)
      end
    end
  end

end
