GithubExperiment::Application.routes.draw do

  root to: 'github#index'
  get '/:username', to: 'github#show'

end
