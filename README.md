Github Experiment
========

Developed using:

* Ruby 2.1.1
* Rails 4.0.4

To run the app:

1. `clone [REPO_URL]`
2. `bundle install`
3. Set an environment variable `ENV['github_token']` with your Github application token
4. `bundle exec rails server`

When you run the tests Rspec will automatically skip slow tests. To run all the tests:

    slow=true rspec
